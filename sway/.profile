if [[ -z $DISPLAY ]] && [[ $(tty) = /dev/tty1 ]]; then
  export MOZ_ENABLE_WAYLAND=1
  export BEMENU_BACKEND=wayland
	XKB_DEFAULT_LAYOUT=us exec sway
fi

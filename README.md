# Dotfiles

Setup:
```
git clone
cd dotfiles
stow bash qutebrowser ranger sh sway tmux vim zsh alacritty
```

tmuxp config override for default tmux file: `stow --override=.config/alacritty/alacritty.yml.d/15-tmux.yml tmuxp`

Created by: Cole Teeter <cole.teeter@gmail.com>

# Source zgen plugin from zgen-git package
source /usr/share/zsh/share/zgen.zsh
# if the init scipt doesn't exist
if ! zgen saved; then
    echo "Creating a zgen save"
    # https://github.com/tarjoilija/zgen
    zgen load unixorn/autoupdate-zgen
    zgen load jreese/zsh-titles
    # zgen load dezza/zsh-titles
    zgen load mjrafferty/apollo-zsh-theme
    zgen load 0b10/cheatsheet
    zgen load "MichaelAquilina/zsh-auto-notify"
    zgen load "zsh-users/zsh-autosuggestions"

    # bulk load
    zgen loadall <<EOPLUGINS
       /usr/share/zsh/plugins/alias-tips/alias-tips.plugin.zsh
       /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
EOPLUGINS
    # ^ can't indent this EOPLUGINS

    # save all to init script
    zgen save
fi

# Add fzf
source /usr/share/fzf/key-bindings.zsh
source /usr/share/fzf/completion.zsh

# fh - repeat history
fh() {
  print -z $( ([ -n "$ZSH_NAME" ] && fc -l 1 || history) | fzf +s --tac | sed -r 's/ *[0-9]*\*? *//' | sed -r 's/\\/\\\\/g')
}

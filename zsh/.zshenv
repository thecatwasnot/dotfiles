# zshenv is read on login & interactive shells

export PATH="$PATH:$HOME/.local/bin:$HOME/.local/scripts"
export EDITOR="/usr/bin/vim"



" Smarter tab line from airline
let g:airline#extensions#tabline#enabled = 1
" Straight separators
let g:airline#extensions#tabline#left_sep = ' '
let g:airline#extensions#tabline#left_alt_sep = '|'
let g:tmuxline_powerline_separators = 0
" Tmuxline theme
let g:tmuxline_preset = {
      \'a'    : '#S',
      \'win'  : ['#I', '#W'],
      \'cwin' : ['#I', '#W', '#F'],
      \'y'    : '#{prefix_highlight}',
      \'z'    : '#H'}

let g:netrw_banner = 0
let g:netrw_liststyle = 3
let g:netrw_browse_split = 4
let g:netrw_winsize = 20

let g:netrw_alto = 1
let g:netrw_altv = 1

augroup ProjectDrawer
  autocmd!
  autocmd VimEnter * :Lexplore
augroup END

nnoremap <Leader>e :Lexplore<cr>


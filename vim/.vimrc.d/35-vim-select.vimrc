if exists("g:loaded_select")
    nmap <leader>fe <Plug>(SelectFile)
    nmap <leader>ff <Plug>(SelectProjectFile)
    nmap <leader>fp <Plug>(SelectProject)
    nmap <leader>b <Plug>(SelectBuffer)
    nmap <leader>m <Plug>(SelectMRU)
    nmap <leader>h <Plug>(SelectHelp)
    nmap <leader>; <Plug>(SelectCmd)
    nmap <leader>/ <Plug>(SelectBufLine)
endif

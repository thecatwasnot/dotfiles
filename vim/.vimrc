" plugpack.vim to manage vim 8 packages like vim-plug
call plugpac#begin()
Pack 'tpope/vim-sensible'
Pack 'tpope/vim-repeat'
Pack 'tpope/vim-commentary'
Pack 'kshenoy/vim-signature'
Pack 'tpope/vim-surround'
Pack 'habamax/vim-select' 

Pack 'vim-airline/vim-airline'
Pack 'edkolev/tmuxline.vim'
Pack 'deviantfero/wpgtk.vim'

Pack 'tmux-plugins/vim-tmux-focus-events'
Pack 'roxma/vim-tmux-clipboard'
Pack 'christoomey/vim-tmux-navigator'
Pack 'benmills/vimux'
Pack 'voldikss/vim-floaterm'

Pack 'junegunn/fzf.vim'
Pack 'vimwiki/vimwiki', { 'branch': 'dev' }
Pack 'tools-life/taskwiki'
Pack 'powerman/vim-plugin-AnsiEsc'
Pack 'majutsushi/tagbar'
Pack 'farseer90718/vim-taskwarrior'

Pack 'ledger/vim-ledger'
call plugpac#end()

for fpath in split(globpath('~/.vimrc.d/','*.vimrc'), '\n')
	exe 'source' fpath
endfor

set -g default-terminal screen-256color

# rebind C-b -> C-a because that's a reach
set -g prefix C-a
unbind-key C-b
bind-key C-a send-prefix

setw -g mode-keys vi

# Change splits to be more intuitive
bind | split-window -h -c "#{pane_current_path}"
bind - split-window -v -c "#{pane_current_path}"

# Zero is allll the way over there
set -g base-index 1
setw -g pane-base-index 1

set -g focus-events on
bind-key m set mouse\; display-message "Mouse mode: #{?mouse,on,off}"

# <prefix> r to reload this file
bind r source-file ~/.tmux.conf \; display "conf file loaded."

# Make <prefix> x just kill a pane without asking
unbind x
bind x kill-pane
bind X kill-session

# Resize panes with vim keys
bind-key -r K resize-pane -U
bind-key -r J resize-pane -D
bind-key -r H resize-pane -L
bind-key -r L resize-pane -R

# tmux plugins (keep at bottom)
# add new plugin with
# set -g @plugin 'plugin'
# Install   - C-a + I
# Update    - C-a + U
# Uninstall - C-a A-u
set -g @plugin 'tmux-plugins/tpm'
set -g @plugin 'tmux-plugins/tmux-sensible'
set -g @plugin 'christoomey/vim-tmux-navigator'
set -g @plugin 'tmux-plugins/tmux-prefix-highlight'
set -g @plugin 'tmux-plugins/tmux-yank'
set -g @plugin 'woodstok/tmux-butler'

# configure tmux-prefix-highlight
set -g @prefix_highlight_fg 'fg=color0,BOLD' # default is 'colour231'
set -g @prefix_highlight_bg 'bg=color12'  # default is 'colour04'

if-shell "test -f ~/.tmux-prompt.conf" "source ~/.tmux-prompt.conf"

if "test ! -d ~/.tmux/plugins/tpm" \
   "run 'git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm && ~/.tmux/plugins/tpm/bin/install_plugins'"
run -b '~/.tmux/plugins/tpm/tpm'
